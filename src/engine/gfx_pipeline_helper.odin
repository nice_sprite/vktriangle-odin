package engine;
import vk "vendor:vulkan"
import "core:log"

PipelineBuilder :: struct {
    m_shaderStages:           [dynamic]vk.PipelineShaderStageCreateInfo,
    m_inputAssembly:          vk.PipelineInputAssemblyStateCreateInfo,
    m_rasterizer:             vk.PipelineRasterizationStateCreateInfo,
    m_colorBlendAttachment:   vk.PipelineColorBlendAttachmentState,
    m_multisampling:          vk.PipelineMultisampleStateCreateInfo,
    m_pipelineLayout:         vk.PipelineLayout,
    m_depthStencil:           vk.PipelineDepthStencilStateCreateInfo,
    m_renderInfo:             vk.PipelineRenderingCreateInfo,
    m_colorAttachmentformat:  vk.Format,
}

pipeline_builder_init :: proc() -> PipelineBuilder {
    builder := PipelineBuilder {}
    pipeline_builder_clear(&builder)
    builder.m_shaderStages = make([dynamic]vk.PipelineShaderStageCreateInfo)
    return builder
}

pipeline_builder_build :: proc(using builder: ^PipelineBuilder, device: vk.Device) -> vk.Pipeline {
    // make viewport state from our stored viewport and scissor.
    // at the moment we wont support multiple viewports or scissors
    viewportState := vk.PipelineViewportStateCreateInfo {
        sType = .PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        pNext = nil,
        viewportCount = 1,
        scissorCount = 1,
    }

    // setup dummy color blending. We arent using transparent objects yet
    // the blending is just "no blend", but we do write to the color attachment
    colorBlending := vk.PipelineColorBlendStateCreateInfo {
        sType = .PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        pNext = nil,
        logicOpEnable = false,
        logicOp = .COPY,
        attachmentCount = 1,
        pAttachments = &m_colorBlendAttachment,
    }

    //completely clear VertexInputStateCreateInfo, as we have no need for it
    vertexInputInfo := vk.PipelineVertexInputStateCreateInfo { 
        sType = .PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
    }

    pipelineInfo := vk.GraphicsPipelineCreateInfo {
        sType = .GRAPHICS_PIPELINE_CREATE_INFO,
        pNext = &m_renderInfo, // this is part of the dynamic rendering extension
        stageCount = u32(len(m_shaderStages)),
        pStages = raw_data(m_shaderStages),
        pVertexInputState = &vertexInputInfo,
        pInputAssemblyState = &m_inputAssembly,
        pViewportState = &viewportState,
        pRasterizationState = &m_rasterizer,
        pMultisampleState = &m_multisampling,
        pColorBlendState = &colorBlending,
        pDepthStencilState = &m_depthStencil,
        layout = m_pipelineLayout,
    }

    // setup dynamic state
    dynamicStates := []vk.DynamicState {.VIEWPORT, .SCISSOR}
    dynamicInfo := vk.PipelineDynamicStateCreateInfo {
        sType = .PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        pDynamicStates = raw_data(dynamicStates),
        dynamicStateCount = u32(len(dynamicStates)),
    }

    pipelineInfo.pDynamicState = &dynamicInfo

    newPipeline: vk.Pipeline
    log.debug("creating graphics pipeline")
    vk_check(vk.CreateGraphicsPipelines(device, {}, 1, &pipelineInfo, nil, &newPipeline))
    return newPipeline
}


pipeline_builder_set_shaders :: proc(using builder: ^PipelineBuilder, vertexShader, fragmentShader: vk.ShaderModule) {
    clear(&m_shaderStages)

    vertexInfo := vk.PipelineShaderStageCreateInfo {
        sType = .PIPELINE_SHADER_STAGE_CREATE_INFO,
        pNext = nil,
        stage = {.VERTEX},
        module = vertexShader,
        pName = "main",
    }

    fragInfo := vk.PipelineShaderStageCreateInfo {
        sType = .PIPELINE_SHADER_STAGE_CREATE_INFO,
        pNext = nil,
        stage = {.FRAGMENT},
        module = fragmentShader,
        pName = "main",
    }

    append(&m_shaderStages, vertexInfo, fragInfo)

}

pipeline_builder_set_input_topo :: proc(using builder: ^PipelineBuilder, topo: vk.PrimitiveTopology) {
    m_inputAssembly.topology = topo
    m_inputAssembly.primitiveRestartEnable = false
}

pipeline_builder_set_polygon_mode :: proc(using builder: ^PipelineBuilder, mode: vk.PolygonMode) {
    m_rasterizer.polygonMode = mode
    m_rasterizer.lineWidth = 1.0
}

pipeline_builder_set_cull_mode :: proc(using builder: ^PipelineBuilder, cullFlags: vk.CullModeFlags, frontFace: vk.FrontFace) {
    m_rasterizer.cullMode = cullFlags 
    m_rasterizer.frontFace = frontFace
}

pipeline_builder_set_multisampling_none :: proc(using builder: ^PipelineBuilder) {
    m_multisampling.sampleShadingEnable = false
    m_multisampling.rasterizationSamples = {._1}
    m_multisampling.minSampleShading = 1.0
    m_multisampling.pSampleMask = nil
    m_multisampling.alphaToOneEnable = false
    m_multisampling.alphaToCoverageEnable = false 
}

pipeline_builder_disable_blending :: proc(using builder: ^PipelineBuilder) {
    m_colorBlendAttachment.colorWriteMask = {.R, .G, .B, .A}
    m_colorBlendAttachment.blendEnable = false
}

pipeline_builder_set_color_attachment_format :: proc(using builder: ^PipelineBuilder, format: vk.Format) {
    m_colorAttachmentformat = format
    m_renderInfo.colorAttachmentCount = 1
    m_renderInfo.pColorAttachmentFormats = &m_colorAttachmentformat
}

pipeline_builder_set_depth_format :: proc(using builder: ^PipelineBuilder, format: vk.Format) {
    m_renderInfo.depthAttachmentFormat = format
}

pipeline_builder_disable_depthtest :: proc(using builder: ^PipelineBuilder) {
    m_depthStencil.depthTestEnable = false
	m_depthStencil.depthWriteEnable = false
	m_depthStencil.depthCompareOp = .NEVER
	m_depthStencil.depthBoundsTestEnable = false
	m_depthStencil.stencilTestEnable = false
	m_depthStencil.front = {}
	m_depthStencil.back = {}
	m_depthStencil.minDepthBounds = 0
	m_depthStencil.maxDepthBounds= 1
}

// frees the m_shaderStages array
pipeline_builder_clear :: proc(using builder: ^PipelineBuilder) {
    clear(&m_shaderStages)
    m_inputAssembly = { sType = .PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO }
	m_rasterizer = { sType = .PIPELINE_RASTERIZATION_STATE_CREATE_INFO }
	m_colorBlendAttachment = {}
	m_multisampling = { sType = .PIPELINE_MULTISAMPLE_STATE_CREATE_INFO }
	m_pipelineLayout = {}
	m_depthStencil = { sType = .PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO }
	m_renderInfo = { sType = .PIPELINE_RENDERING_CREATE_INFO }
}
