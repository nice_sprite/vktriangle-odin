#!/usr/bin/python3
import glob
import subprocess
import os
import argparse
import platform
import pprint


parser =  argparse.ArgumentParser()
parser.add_argument('-r', '--run')
parser.add_argument('-release')
parser.add_argument('-window_system')
parser.add_argument('-log_mode')

def compile_shaders(args):
    print('='*5, 'COMPILING SHADERS', '='*5)
    for shader_src in glob.glob('./shaders/*.glsl'): 
        filename = os.path.basename(shader_src)
        print(f'\tcompiling {filename}...\n')
        stage_str = filename.split('.')[1]
        output_name_str = filename.split('.')[0] + '.' + stage_str
        result = subprocess.run([ 'glslc',
                        # '--target-env=vulkan1.3',
                        f'-fshader-stage={stage_str}',
                        '-g',
                        shader_src,
                        '-o',
                        f'./bin/shaders/{output_name_str}.spv'], 
                            capture_output=True)
        if result.returncode != 0:
            print(result.stderr)
        
        return result.returncode


def compile_engine(args):
    print('='*5, 'COMPILING ENGINE', '='*5)
    buildCmd = ['odin', 'build', 'src', 
                    '-collection:engine=src/engine', 
                    '-collection:external=external', 
                    '-out:bin/engine',
                    '-extra-linker-flags:-lstdc++'
                    ]
    if args['release']: 
        buildCmd.append('-o:speed')
        buildCmd.append('-no-bounds-check')
        buildCmd.append('-disable-assert')
    else: 
        buildCmd.append('-debug')

    if args['os'] == 'Linux' and args['window_system'] == "x11": 
        buildCmd.append('-define:WAYLAND=false')
        buildCmd.append('-define:X11=true')

    if args['os'] == 'Linux' and args['window_system'] == "Wayland": 
        buildCmd.append('-define:WAYLAND=true')
        buildCmd.append('-define:X11=false')
    success = subprocess.run(buildCmd).returncode == 0
    if success: print('\tSuccess.')
    if args['run'] and success: 
        subprocess.run('./bin/engine')



args = parser.parse_args()
print(args)


# do some fingerprinting
settings = {
        'os': platform.system(),
        'release': args.release, 
        'log_mode': args.log_mode,
        'compile_shaders': True,
        'run': True # run immediately after compilation
}


# window_system arg overrides the automatic detection
if args.window_system == None:
    if platform.system() == "Linux":
        settings['window_system'] = os.environ.get('XDG_SESSION_TYPE')
    else: 
        settings['window_system'] = "win32"

pprint.pprint(settings)

if compile_shaders(settings) == 0:
    compile_engine(settings)


