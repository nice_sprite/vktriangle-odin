package engine;

Vec4 :: [4]f32
Vec3 :: [3]f32
Vec2 :: [2]f32

Mat4 :: distinct matrix[4, 4]f32
Mat3 :: distinct matrix[3, 3]f32
