package engine;
import "core:log"
import vk "vendor:vulkan"
import vma "external:odin_vma"

/* 
design idea:
store the device, and vma since that is what everything is allocated from

the rawptr can be a vulkan handle, or it can be a actual pointer- maybe use an enum to differentiate the 2 if it matters or is significantly clearer 

the device and vma_alloc params dont need to be used but *most* things will need them to delete the resource
delete_fn :: proc(device: vk.Device, vma_alloc: vma.Allocator, type: VK_OBJECT_TYPE, object: rawptr)

Since vulkan cares about order of deletion we cant use a hashmap on the type 
(type, [dynamic]rawptr array, delete proc)
*/ 

Resource :: union {
    vk.Handle,
    vma.Handle,
    vma.Allocator,
    rawptr, 
}


DeletionQueue :: struct {
    deletors: [dynamic]DeleteInfo,
    device: vk.Device,
    allocator: vma.Allocator,
}

init_deletion_queue :: proc( device: vk.Device, allocator: vma.Allocator) -> DeletionQueue {
    q := DeletionQueue {
        deletors = make([dynamic]DeleteInfo, 0, 16),
        device = device,
        allocator = allocator,
    }
    return q
}

push :: proc(queue: ^DeletionQueue, info: DeleteInfo) {
    append(&queue.deletors, info);
}

DeleteInfo :: struct {
    delete_fn: proc(^DeleteInfo, vk.Device, vma.Allocator) -> bool,
    handle: Resource,  // the handle to the object
    sType: vk.StructureType, // might not need
    object_name: string,
}

wipe_all :: proc(queue: ^DeletionQueue,) -> bool {
    #reverse for &deletor in queue.deletors {
        log.debug("deleting ", deletor.object_name)
        log.debug("handle = ", deletor.handle)
        if !deletor->delete_fn(queue.device, queue.allocator) {
            log.errorf("deletor \'%s\' had an error", deletor.object_name);
            return false;
        }
    }
    clear(&queue.deletors)
    return true;
}

