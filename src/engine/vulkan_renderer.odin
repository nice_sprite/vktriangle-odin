package engine
import "core:fmt"
import "core:log"
import "core:strings"
import "core:strconv"
import "core:runtime"
import "core:math"
import "core:simd"
import sa "core:container/small_array"
import vk "vendor:vulkan"
import vma "external:odin_vma"
import imgui "external:odin-imgui"
import imgui_vk "external:odin-imgui/imgui_impl_vulkan"
import imgui_glfw "external:odin-imgui/imgui_impl_glfw"
import "vendor:glfw"

VALIDATION_LAYERS := []cstring{"VK_LAYER_KHRONOS_validation", "VK_LAYER_RENDERDOC_Capture"}
DEVICE_EXTENSIONS := []cstring{vk.KHR_SWAPCHAIN_EXTENSION_NAME}

/// forwards the result 
vk_check :: proc(vk_result: vk.Result, loc := #caller_location) -> vk.Result {
    if vk_result != .SUCCESS {
        log.panic(loc, vk_result)
    } else {
        //log.info(vk_result, location = loc)
    }
    return vk_result
}

FRAME_OVERLAP :: 2

QueueFamilys :: struct {
    graphics: u32,
    compute: u32,
    transfer: u32,
}

RenderImage :: struct {
    image: vk.Image,
    view: vk.ImageView,
    allocation: vma.Allocation,
    extent: vk.Extent3D,
    format: vk.Format,
}

ComputePushConstants :: struct {
   data1: Vec4,
   data2: Vec4,
   data3: Vec4,
   data4: Vec4,
}

FrameData :: struct {
    cmd_pool: vk.CommandPool,
    cmd_buffer: vk.CommandBuffer,
    swapchain_sema, render_sema: vk.Semaphore,
    render_fence: vk.Fence,
    m_deleteQueue: DeletionQueue,
}

Vulkan_Renderer :: struct {
    m_instance: vk.Instance,
    m_device: vk.Device,
    m_physicalDevice: vk.PhysicalDevice,
    m_physicalDeviceProperties: vk.PhysicalDeviceProperties,
    m_physicalDeviceFeatures: vk.PhysicalDeviceFeatures,
    m_debugMessenger: vk.DebugUtilsMessengerEXT,
    m_graphicsQueueIndex: u32,
    m_graphicsQueue: vk.Queue,

    m_window: glfw.WindowHandle,
    m_windowWidth, m_windowHeight: u32,
    m_surface: vk.SurfaceKHR,
    m_swapchain: vk.SwapchainKHR,
    m_swapchainImageFormat: vk.Format,
    m_swapchainImages: [dynamic]vk.Image,
    m_swapchainImageViews: [dynamic]vk.ImageView,
    m_swapchainExtent: vk.Extent2D,
    m_renderImage: RenderImage,
    m_renderImageExtent: vk.Extent2D,
    
    m_perFrameData: [FRAME_OVERLAP]FrameData,
    m_frameIndex: int,

    m_rendererContext: runtime.Context,
    m_allocator: vma.Allocator,

    m_deleteQueue: DeletionQueue,

    m_globalDescriptorPool : vk.DescriptorPool,
    m_drawImageDescriptors : vk.DescriptorSet,
    m_drawImageDescriptorLayout : vk.DescriptorSetLayout,
    m_gradientPipeline : vk.Pipeline,
    m_gradientPipelineLayout : vk.PipelineLayout,

    // stuff for immediate submission
    m_immediateFence: vk.Fence,
    m_immediateCmdBuffer: vk.CommandBuffer,
    m_immediateCmdPool: vk.CommandPool,

    m_trianglePipelineLayout: vk.PipelineLayout,
    m_trianglePipeline: vk.Pipeline,
}


init_renderer :: proc(renderer: ^Vulkan_Renderer, window: glfw.WindowHandle, instance_extensions: []cstring) {
    renderer.m_window = window
    renderer.m_rendererContext = context
    renderer.m_frameIndex = 0
    context.user_ptr = &renderer.m_instance
    gpa :: proc(p: rawptr, name: cstring) {
        (cast(^rawptr)p)^ = glfw.GetInstanceProcAddress((^vk.Instance)(context.user_ptr)^, name)
    }
    vk.load_proc_addresses(gpa)

    extensions := available_instance_extensions()
    layers := available_instance_layers()

    log.debug("Available Instance Extensions:")
    for e in extensions {
        log.debugf("\t%s\n", e.extensionName)
    }

    log.debug("Available Instance Layers:")
    for l in layers {
        log.debugf("\t%s\n", l.layerName)
    }

    for e in instance_extensions {
        log.debugf("required: %s\n", e)
    }

    if !verify_extensions(instance_extensions, extensions) {
        log.error("missing extensions")
    } else {
        log.info("no missing extensions")
    }

    if !verify_layers(VALIDATION_LAYERS, layers) {
        log.error("missing layers")
    } else {
        log.info("no missing layers")
    }

    app_info: vk.ApplicationInfo
    app_info.sType = .APPLICATION_INFO
    app_info.apiVersion = vk.API_VERSION_1_3
    app_info.pEngineName = "no name"
    app_info.engineVersion = vk.MAKE_VERSION(0, 0, 1)
    app_info.applicationVersion = app_info.engineVersion

    create_info: vk.InstanceCreateInfo
    create_info.sType = .INSTANCE_CREATE_INFO
    create_info.pApplicationInfo = &app_info
    create_info.enabledLayerCount = u32(len(VALIDATION_LAYERS))
    create_info.ppEnabledLayerNames = raw_data(VALIDATION_LAYERS)
    create_info.enabledExtensionCount = u32(len(instance_extensions))
    create_info.ppEnabledExtensionNames = raw_data(instance_extensions[:])

    vk_check(vk.CreateInstance(&create_info, nil, &renderer.m_instance)) 
    
    vk.load_proc_addresses(gpa)
    
    when ODIN_DEBUG {
        // Create the debug logger 
        log.info("Creating debug logger...")
        vkdebug_callback :: proc "system" (
            messageSeverity: vk.DebugUtilsMessageSeverityFlagsEXT,
            messageTypes: vk.DebugUtilsMessageTypeFlagsEXT, 
            pCallbackData: ^vk.DebugUtilsMessengerCallbackDataEXT, 
            pUserData: rawptr) -> b32 {

            context = (cast(^runtime.Context)pUserData)^
            log.error(pCallbackData.pMessage)
            return false
        }
        dci: vk.DebugUtilsMessengerCreateInfoEXT
        dci.sType = .DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT
        dci.messageSeverity = { .VERBOSE, .ERROR, .WARNING, .INFO }
        dci.messageType = { .GENERAL, .PERFORMANCE, .VALIDATION }
        dci.pfnUserCallback = vkdebug_callback
        dci.pUserData = &renderer.m_rendererContext
        vk_check(vk.CreateDebugUtilsMessengerEXT(renderer.m_instance, &dci, nil, &renderer.m_debugMessenger))
    }
    
    // create a surface, and then a physical device
    vk_check(glfw.CreateWindowSurface(renderer.m_instance, renderer.m_window, nil, &renderer.m_surface))

    vk13 : vk.PhysicalDeviceVulkan13Features
    vk13.sType = .PHYSICAL_DEVICE_VULKAN_1_3_FEATURES
    vk13.dynamicRendering = true
    vk13.synchronization2 = true
    
    vk12 : vk.PhysicalDeviceVulkan12Features
    vk12.sType = .PHYSICAL_DEVICE_VULKAN_1_2_FEATURES
    vk12.bufferDeviceAddress = true
    vk12.descriptorIndexing = true

    init_device(renderer, vk13, vk12, DEVICE_EXTENSIONS, VALIDATION_LAYERS, vk.MAKE_VERSION(1, 3, 0))
    init_vma(renderer)
    
    // creating main deletion queue
    renderer.m_deleteQueue = init_deletion_queue(renderer.m_device, renderer.m_allocator)

    push(&renderer.m_deleteQueue, DeleteInfo {
        handle = renderer.m_allocator,
        object_name = "VulkanMemoryAllocator",
        delete_fn = proc(self: ^DeleteInfo, device: vk.Device, alloc: vma.Allocator) -> bool {
            vma.DestroyAllocator(self.handle.(vma.Allocator));
            return true;
        },
    });

    vk.GetPhysicalDeviceFeatures(renderer.m_physicalDevice, &renderer.m_physicalDeviceFeatures)
    log.debugf("Selected PhysicalDevice %v %s", renderer.m_physicalDevice, renderer.m_physicalDeviceProperties.deviceName)

    w, h := glfw.GetWindowSize(renderer.m_window)
    renderer.m_windowWidth = u32(w)
    renderer.m_windowHeight = u32(h)

    init_swapchain(renderer, 0)
    
    // Create command pool
    
    log.info("creating command buffers...")
    init_commands(renderer)
    
    log.info("creating sync objects")
    init_sync(renderer)
    init_descriptors(renderer)
    init_pipelines(renderer)
    imgui_init(renderer)
        
}

deinit_renderer :: proc (using renderer: ^Vulkan_Renderer) {
    log.debug("Cleaning up Vulkan_Renderer...")
    vk.DeviceWaitIdle(m_device)
    wipe_all(&renderer.m_deleteQueue);

    //vma.DestroyAllocator(m_allocator)
    deinit_sync(renderer)
    deinit_commands(renderer)

    for view in m_swapchainImageViews do vk.DestroyImageView(m_device, view, nil)

    vk.DestroySwapchainKHR(m_device, m_swapchain, nil)
    vk.DestroySurfaceKHR(m_instance, m_surface, nil)
    vk.DestroyDevice(m_device, nil)
    vk.DestroyDebugUtilsMessengerEXT(m_instance, renderer.m_debugMessenger, nil)
    vk.DestroyInstance(m_instance, nil)
    log.debug("Done.")
}

available_instance_extensions :: proc() -> []vk.ExtensionProperties {
    instance_ext_count := u32(0)
    vk_check(vk.EnumerateInstanceExtensionProperties(nil, &instance_ext_count, nil))

    extensions := make([]vk.ExtensionProperties, instance_ext_count)
    vk_check(vk.EnumerateInstanceExtensionProperties(nil, &instance_ext_count, raw_data(extensions)))
    return extensions
}

available_instance_layers :: proc() -> []vk.LayerProperties{
    layer_count := u32(0)
    vk_check(vk.EnumerateInstanceLayerProperties(&layer_count, nil))
    layers := make([]vk.LayerProperties, layer_count)
    vk_check(vk.EnumerateInstanceLayerProperties(&layer_count, raw_data(layers)))
    return layers
}

verify_extensions :: proc(required: []cstring, available: []vk.ExtensionProperties) -> bool {
    outer: for &rext in required {
        found := false
        for &ext in available {
            if cstring(&ext.extensionName[0]) == rext {
                found = true
                continue outer
            }
        }
        log.error("missing ", rext)
        return false
    }
    return true
}

verify_layers :: proc(required: []cstring, available: []vk.LayerProperties) -> bool {
    outer: for &rext in required {
        found := false
        for &av in available {
            if cstring(raw_data(av.layerName[:])) == rext {
                found = true
                continue outer
            }
        }
        log.error("missing ", rext)
        return false
    }
    return true
}


// TODO refactor this so that it directly writes the results to the Vulkan_Renderer struct.
init_device :: proc(renderer: ^Vulkan_Renderer,
    feat_13: vk.PhysicalDeviceVulkan13Features,
    feat_12: vk.PhysicalDeviceVulkan12Features,
    required_extensions: []cstring,
    required_layers: []cstring,
    min_ver: u32) {

    count: u32
    vk_check(vk.EnumeratePhysicalDevices(renderer.m_instance, &count, nil))
    pdevs := make([]vk.PhysicalDevice, count)
    defer delete(pdevs)
    vk_check(vk.EnumeratePhysicalDevices(renderer.m_instance, &count, raw_data(pdevs)))

    gpu_index: i32 = -1
    m_graphicsQueue_index: i32 = -1
    physical_device_properties : vk.PhysicalDeviceProperties
    for d, i in pdevs {
        properties: vk.PhysicalDeviceProperties
        queue_family_count: u32
        
        vk.GetPhysicalDeviceProperties(d, &properties)
        vk.GetPhysicalDeviceQueueFamilyProperties(d, &queue_family_count, nil)
        queue_family_properties := make([]vk.QueueFamilyProperties, queue_family_count)
        vk.GetPhysicalDeviceQueueFamilyProperties(d, &queue_family_count, raw_data(queue_family_properties))
        
        for qfp, queue_index in queue_family_properties {
            supports_present: b32
            vk_check(vk.GetPhysicalDeviceSurfaceSupportKHR(d, u32(queue_index), renderer.m_surface, &supports_present))

            if .GRAPHICS in qfp.queueFlags && supports_present && properties.deviceType == .DISCRETE_GPU {
                m_graphicsQueue_index = i32(queue_index)
                gpu_index = i32(i)
                physical_device_properties = properties
                break
            }
        }
        if m_graphicsQueue_index < 0 do log.panic("did not find a suitable queue that graphics and presentation")
    }
    if gpu_index == -1 do log.panic("gpu heuristic failed to find a suitable card")

    device_extension_count: u32
    vk_check(vk.EnumerateDeviceExtensionProperties(pdevs[gpu_index], nil, &device_extension_count, nil))
    device_extensions := make([]vk.ExtensionProperties, device_extension_count)
    vk_check(vk.EnumerateDeviceExtensionProperties(pdevs[gpu_index], nil, &device_extension_count, raw_data(device_extensions)))

    device_layer_count: u32
    vk_check(vk.EnumerateDeviceLayerProperties(pdevs[gpu_index], &device_layer_count, nil))
    device_layers := make([]vk.LayerProperties, device_layer_count)
    vk_check(vk.EnumerateDeviceLayerProperties(pdevs[gpu_index], &device_layer_count, raw_data(device_layers)))
    
    for e in device_extensions do log.debugf("device_ext: %s\n", e.extensionName)
    if !verify_extensions(required_extensions, device_extensions) {
        log.panic("device extensions are missing!")
    }
    
    if !verify_layers(required_layers, device_layers) {
        log.panic("device layers are missing!")
    }
    

    // create a queue
    queue_priority : f32 = 1.0
    queue_info: vk.DeviceQueueCreateInfo
    queue_info.sType = .DEVICE_QUEUE_CREATE_INFO
    queue_info.queueFamilyIndex = u32(m_graphicsQueue_index)
    queue_info.queueCount = 1
    queue_info.pQueuePriorities = &queue_priority
    
    // TODO; cast the passed-in 1.2 and 1.3 features to byte array, and check for compatitibility with the queried available features
    features_1_3 := feat_13
    features_1_2 := feat_12

    features_1_2.pNext = &features_1_3
    
    physical_device_features2 : vk.PhysicalDeviceFeatures2
    physical_device_features2.sType = .PHYSICAL_DEVICE_FEATURES_2
    vk.GetPhysicalDeviceFeatures2(pdevs[gpu_index], &physical_device_features2)
    physical_device_features2.pNext = &features_1_2
    
    device_create_info := vk.DeviceCreateInfo {
        sType = .DEVICE_CREATE_INFO,
        pQueueCreateInfos = &queue_info,
        queueCreateInfoCount = 1,
        enabledExtensionCount = u32(len(required_extensions)),
        ppEnabledExtensionNames = raw_data(required_extensions),
        enabledLayerCount = u32(len(required_layers)),
        ppEnabledLayerNames = raw_data(required_layers),
        pNext = &physical_device_features2,
    }
    
    vk_check(vk.CreateDevice(pdevs[gpu_index], &device_create_info, nil, &renderer.m_device))
    vk.load_proc_addresses_device(renderer.m_device) 
    
    vk.GetDeviceQueue(renderer.m_device, u32(m_graphicsQueue_index), 0, &renderer.m_graphicsQueue)
    renderer.m_physicalDeviceProperties =  physical_device_properties
    renderer.m_graphicsQueueIndex = u32(m_graphicsQueue_index)
    renderer.m_physicalDevice = pdevs[gpu_index]
}

init_swapchain :: proc(renderer: ^Vulkan_Renderer, old_swapchain: vk.SwapchainKHR) {
    surface_format_count: u32
    vk_check(vk.GetPhysicalDeviceSurfaceFormatsKHR(renderer.m_physicalDevice, renderer.m_surface, &surface_format_count, nil))
    surface_formats := make([]vk.SurfaceFormatKHR, surface_format_count)
    vk_check(vk.GetPhysicalDeviceSurfaceFormatsKHR(renderer.m_physicalDevice, renderer.m_surface, &surface_format_count, raw_data(surface_formats)))
    
    preferred_format : vk.SurfaceFormatKHR
    preferred_format.format = .B8G8R8A8_UNORM
    surface_supports_format := false

    for surf_format in surface_formats {
        if surf_format.format == preferred_format.format {
            surface_supports_format = true
            preferred_format = surf_format // copy over the colorspace 
            break
        }
    }
    
    if !surface_supports_format do log.panic("surface does not support ", preferred_format)
    else do log.debug("surface supports ", preferred_format)

    surface_properties : vk.SurfaceCapabilitiesKHR
    vk_check(vk.GetPhysicalDeviceSurfaceCapabilitiesKHR(renderer.m_physicalDevice, renderer.m_surface, &surface_properties))
    
    desired_image_count := clamp(surface_properties.minImageCount+1, surface_properties.minImageCount, surface_properties.maxImageCount)
    pre_transform : vk.SurfaceTransformFlagsKHR

    if .IDENTITY in surface_properties.currentTransform do pre_transform = { .IDENTITY }
    else do pre_transform = surface_properties.currentTransform
    
    composite_alpha : vk.CompositeAlphaFlagsKHR
    if .OPAQUE in surface_properties.supportedCompositeAlpha do composite_alpha = {.OPAQUE}
    else if .INHERIT in surface_properties.supportedCompositeAlpha do composite_alpha = {.INHERIT}
    else if .PRE_MULTIPLIED in surface_properties.supportedCompositeAlpha do composite_alpha = {.PRE_MULTIPLIED}
    else if .POST_MULTIPLIED in surface_properties.supportedCompositeAlpha do composite_alpha = {.POST_MULTIPLIED}
    
    log.debug("composite alpha:", composite_alpha)
    
    swapchain_create_info := vk.SwapchainCreateInfoKHR {
        sType = .SWAPCHAIN_CREATE_INFO_KHR,
        surface = renderer.m_surface,
        imageFormat = preferred_format.format,
        imageColorSpace = preferred_format.colorSpace,
        clipped = true,
        minImageCount = desired_image_count,
        imageExtent = {
            width = renderer.m_windowWidth,
            height = renderer.m_windowHeight,
        },
        imageArrayLayers = 1,
        imageUsage = {.COLOR_ATTACHMENT, .TRANSFER_DST},
        imageSharingMode = .EXCLUSIVE,
        preTransform = pre_transform,
        compositeAlpha = composite_alpha,
        presentMode = .FIFO, // TODO change to mailbox but query support for it?
        oldSwapchain = renderer.m_swapchain,
    }

    renderer.m_swapchainExtent.width = renderer.m_windowWidth
    renderer.m_swapchainExtent.height = renderer.m_windowHeight


    vk_check(vk.CreateSwapchainKHR(renderer.m_device, &swapchain_create_info, nil, &renderer.m_swapchain))
    renderer.m_swapchainImageFormat = preferred_format.format

    if renderer.m_swapchainImages == nil {
        renderer.m_swapchainImages = make([dynamic]vk.Image, desired_image_count)
    }

    if renderer.m_swapchainImageViews == nil {
        renderer.m_swapchainImageViews = make([dynamic]vk.ImageView, desired_image_count)
    }

    
    resize(&renderer.m_swapchainImages, int(desired_image_count))
    resize(&renderer.m_swapchainImageViews, int(desired_image_count))

    log.debug("desired image count is", desired_image_count)
    vk_check(vk.GetSwapchainImagesKHR(renderer.m_device, renderer.m_swapchain, &desired_image_count, raw_data(renderer.m_swapchainImages)))
    
    for i := 0; i < int(desired_image_count); i += 1 {
        view_info := vk.ImageViewCreateInfo {
            sType = .IMAGE_VIEW_CREATE_INFO,
            viewType = .D2,
            format = preferred_format.format,
            image = renderer.m_swapchainImages[i],
            subresourceRange = {
                levelCount = 1,
                layerCount = 1,
                baseMipLevel = 0,
                baseArrayLayer = 0,
                aspectMask = {.COLOR},
            },
            components = {
                r = .R,
                g = .G,
                b = .B,
                a = .A,
            },
        }

		image_view : vk.ImageView
		vk_check(vk.CreateImageView(renderer.m_device, &view_info, nil, &renderer.m_swapchainImageViews[i]))
    }

    // create the render texture
    rt_extent:= vk.Extent3D {
        width = renderer.m_windowWidth,
        height = renderer.m_windowHeight,
        depth = 1,
    }

    renderer.m_renderImage.format = .R16G16B16A16_SFLOAT;
    renderer.m_renderImage.extent = rt_extent;

    image_usage_flags := vk.ImageUsageFlags {.TRANSFER_DST, .TRANSFER_SRC, .STORAGE, .COLOR_ATTACHMENT};
    rimg_create_info := init_image_create_info(renderer.m_renderImage.format, image_usage_flags, renderer.m_renderImage.extent); 
    rimg_alloc_info := vma.AllocationCreateInfo {
        usage = .GPU_ONLY,
        requiredFlags = {.DEVICE_LOCAL},
    }

    vk_check(vma.CreateImage(renderer.m_allocator, &rimg_create_info, &rimg_alloc_info, &renderer.m_renderImage.image, &renderer.m_renderImage.allocation, nil))

    // create a view of the image
    rimg_view_info := init_image_view_create_info(renderer.m_renderImage.format, renderer.m_renderImage.image, {.COLOR})
    vk_check(vk.CreateImageView(renderer.m_device, &rimg_view_info, nil, &renderer.m_renderImage.view))

    push(&renderer.m_deleteQueue, DeleteInfo{
        object_name = "RenderImage",
        handle = cast(rawptr)(&renderer.m_renderImage),
        delete_fn = proc(self: ^DeleteInfo, device: vk.Device, allocator: vma.Allocator) -> bool {
            m_renderImage_ptr := self.handle.(rawptr);
            m_renderImage := cast(^RenderImage)m_renderImage_ptr;
            vk.DestroyImageView(device, m_renderImage.view, nil)
            vma.DestroyImage(allocator, m_renderImage.image, m_renderImage.allocation)
            return true;
        },
    });
}


init_commands :: proc(using vulkan: ^Vulkan_Renderer) {
    command_pool_info: vk.CommandPoolCreateInfo
    command_pool_info.sType = .COMMAND_POOL_CREATE_INFO
    command_pool_info.pNext = nil
    command_pool_info.flags = {.RESET_COMMAND_BUFFER}
    command_pool_info.queueFamilyIndex = m_graphicsQueueIndex

    for i := 0; i < FRAME_OVERLAP; i += 1 {
        vk_check(vk.CreateCommandPool(m_device, &command_pool_info, nil, &m_perFrameData[i].cmd_pool))
        
        cmd_buffer_allocate_info: vk.CommandBufferAllocateInfo
        cmd_buffer_allocate_info.sType = .COMMAND_BUFFER_ALLOCATE_INFO
        cmd_buffer_allocate_info.pNext = nil
        cmd_buffer_allocate_info.commandPool = m_perFrameData[i].cmd_pool
        cmd_buffer_allocate_info.commandBufferCount = 1
        cmd_buffer_allocate_info.level = .PRIMARY
        
        vk_check(vk.AllocateCommandBuffers(m_device, &cmd_buffer_allocate_info, &m_perFrameData[i].cmd_buffer))
    }

    // create immediate submission command buffers
    vk_check(vk.CreateCommandPool(m_device, &command_pool_info, nil, &m_immediateCmdPool))
    cmdAllocInfo := vk.CommandBufferAllocateInfo {
        sType = .COMMAND_BUFFER_ALLOCATE_INFO,
        pNext = nil,
        commandPool = m_immediateCmdPool,
        commandBufferCount = 1,
        level = .PRIMARY,
    }

    vk_check(vk.AllocateCommandBuffers(m_device, &cmdAllocInfo, &m_immediateCmdBuffer))
    push(&m_deleteQueue, DeleteInfo {
        handle = transmute(vk.Handle)m_immediateCmdPool,
        object_name = "immediate command pool",
        delete_fn = proc(self: ^DeleteInfo, device: vk.Device, allocator: vma.Allocator) -> bool {
            cmdPool := transmute(vk.CommandPool)self.handle.(vk.Handle)
            vk.DestroyCommandPool(device, cmdPool, nil)
            return true 
        },
    })
}

init_sync :: proc(using vulkan: ^Vulkan_Renderer) {
    for _, i in m_perFrameData {
        fence_create_info := vk.FenceCreateInfo{
            sType = .FENCE_CREATE_INFO, 
            flags = {.SIGNALED},
            pNext = nil, 
        }
        
        semaphore_create_info := vk.SemaphoreCreateInfo {
            sType = .SEMAPHORE_CREATE_INFO,
        }

        vk_check(vk.CreateFence(m_device, &fence_create_info, nil, &m_perFrameData[i].render_fence))
        vk_check(vk.CreateSemaphore(m_device, &semaphore_create_info, nil, &m_perFrameData[i].swapchain_sema))
        vk_check(vk.CreateSemaphore(m_device, &semaphore_create_info, nil, &m_perFrameData[i].render_sema))
    }

    fenceCreateInfo := vk.FenceCreateInfo{
        sType = .FENCE_CREATE_INFO, 
        flags = {.SIGNALED},
        pNext = nil, 
    }

    // create sync structures for immediates 
    vk_check(vk.CreateFence(m_device, &fenceCreateInfo, nil, &m_immediateFence))
}

deinit_sync :: proc(using vulkan: ^Vulkan_Renderer) {
    for _, i in m_perFrameData {
        vk.DestroySemaphore(m_device, m_perFrameData[i].render_sema, nil)
        vk.DestroySemaphore(m_device, m_perFrameData[i].swapchain_sema, nil)
        vk.DestroyFence(m_device, m_perFrameData[i].render_fence, nil)
    }
}

deinit_commands :: proc(using vulkan: ^Vulkan_Renderer) {

    for _,i in m_perFrameData {
        vk.DestroyCommandPool(m_device, m_perFrameData[i].cmd_pool, nil)
    }

}



get_current_frame :: proc(using vulkan: ^Vulkan_Renderer) -> ^FrameData {
    return &m_perFrameData[m_frameIndex % FRAME_OVERLAP]
}

draw :: proc(using vulkan: ^Vulkan_Renderer) {
    TIMEOUT :: ~u64(0)
    currentFrameData := get_current_frame(vulkan)
    vk_check(vk.WaitForFences(m_device, 1, &currentFrameData.render_fence, true, TIMEOUT ))
    wipe_all(&currentFrameData.m_deleteQueue)
    vk_check(vk.ResetFences(m_device, 1, &currentFrameData.render_fence))
    swapchain_index : u32

    vk_check(vk.AcquireNextImageKHR(m_device, m_swapchain, TIMEOUT, currentFrameData.swapchain_sema, 0, &swapchain_index)) 
    cmd := currentFrameData.cmd_buffer
    vk_check(vk.ResetCommandBuffer(cmd, {}))
    cmd_buffer_begin_info := vk.CommandBufferBeginInfo {
        sType = .COMMAND_BUFFER_BEGIN_INFO,
        flags = {.ONE_TIME_SUBMIT},
        pInheritanceInfo = nil,
        pNext = nil,
    }
    
    vk_check(vk.BeginCommandBuffer(cmd, &cmd_buffer_begin_info))

    // transition to general so we can write to it
    transition_swapchain_image(
        cmd, 
        m_renderImage.image,
        .UNDEFINED,
        .GENERAL,
    )

    vk.CmdBindPipeline(cmd, .COMPUTE, m_gradientPipeline)
    vk.CmdBindDescriptorSets(cmd, .COMPUTE, m_gradientPipelineLayout, 0, 1, &m_drawImageDescriptors, 0, nil)

    m_renderImageExtent.width = m_windowWidth
    m_renderImageExtent.height = m_windowHeight

    pushConsts := ComputePushConstants{
        data1 = Vec4{0.1, 0.1, 0.1, 1},
        data2 = Vec4{0.1, 0.1, 0.1, 1},
    }
    vk.CmdPushConstants(cmd, m_gradientPipelineLayout, {.COMPUTE}, 0, size_of(ComputePushConstants), &pushConsts)
    vk.CmdDispatch(
        cmd, 
        cast(u32)math.ceil(f32(m_renderImageExtent.width)/ 16.0),
        cast(u32)math.ceil(f32(m_renderImageExtent.height)/ 16.0),
        1,
    )

    // draw using graphics pipeline
    transition_swapchain_image(cmd, m_renderImage.image, .GENERAL, .COLOR_ATTACHMENT_OPTIMAL)
    renderer_draw_geometry(vulkan, cmd)

    transition_swapchain_image(cmd, m_renderImage.image, .COLOR_ATTACHMENT_OPTIMAL, .TRANSFER_SRC_OPTIMAL)
    transition_swapchain_image(cmd, m_swapchainImages[swapchain_index], .UNDEFINED, .TRANSFER_DST_OPTIMAL)

    extent2d_m_renderImage := vk.Extent2D {
        m_renderImage.extent.width,
        m_renderImage.extent.height,
    }
    // m_renderImage.extent, m_renderImage.extent) should be the same for now
    // TODO keep track of the actual extents and update them based on window size
    blit_image( cmd,
                m_renderImage.image,
                m_swapchainImages[swapchain_index],
                extent2d_m_renderImage, 
                m_swapchainExtent,
                )


    transition_swapchain_image(
                cmd,
                m_swapchainImages[swapchain_index],
                .TRANSFER_DST_OPTIMAL,
                .COLOR_ATTACHMENT_OPTIMAL)

    draw_imgui(vulkan, cmd, m_swapchainImageViews[swapchain_index])



    transition_swapchain_image(
                cmd,
                m_swapchainImages[swapchain_index],
                .COLOR_ATTACHMENT_OPTIMAL,
                .PRESENT_SRC_KHR)

    
    vk_check(vk.EndCommandBuffer(cmd))
    
    // submit the command buffer
    cmd_submit_info := init_command_buffer_submit_info(cmd)
    // gotta wait on swapchain to be ready
    wait_info := init_semaphore_submit_info({.COLOR_ATTACHMENT_OUTPUT}, currentFrameData.swapchain_sema)
    signal_info := init_semaphore_submit_info({.ALL_GRAPHICS}, currentFrameData.render_sema)
    submit_info := init_submit_info(&cmd_submit_info, &signal_info, &wait_info)
    
    vk_check(vk.QueueSubmit2(m_graphicsQueue, 1, &submit_info, currentFrameData.render_fence))
    
    // present to the screen!
    present_info := vk.PresentInfoKHR {
        sType = .PRESENT_INFO_KHR,
        pNext = nil,
        pSwapchains = &m_swapchain,
        swapchainCount = 1,
        pWaitSemaphores = &currentFrameData.render_sema,
        waitSemaphoreCount = 1,
        pImageIndices = &swapchain_index,
    }
    
    vk_check(vk.QueuePresentKHR(m_graphicsQueue, &present_info))
    m_frameIndex += 1
}

transition_swapchain_image :: proc(cmd: vk.CommandBuffer, image: vk.Image, curr_layout, new_layout: vk.ImageLayout) {
    image_barrier := vk.ImageMemoryBarrier2 {
        sType = .IMAGE_MEMORY_BARRIER_2,
        pNext = nil,
        srcStageMask = {.ALL_COMMANDS},
        srcAccessMask = {.MEMORY_WRITE},
        dstStageMask = {.ALL_COMMANDS},
        dstAccessMask = {.MEMORY_WRITE, .MEMORY_READ},
        oldLayout = curr_layout,
        newLayout = new_layout,
        image = image,
    }
    
    aspect_mask : vk.ImageAspectFlags
    if new_layout == .DEPTH_ATTACHMENT_OPTIMAL do aspect_mask = {.DEPTH}
    else do aspect_mask = {.COLOR}
    
    image_barrier.subresourceRange = vk.ImageSubresourceRange {
        aspectMask = aspect_mask,
        baseMipLevel = 0,
        levelCount = vk.REMAINING_MIP_LEVELS,
        baseArrayLayer = 0,
        layerCount = vk.REMAINING_ARRAY_LAYERS,
    } 
    
    dependency_info := vk.DependencyInfo {
        sType = .DEPENDENCY_INFO,
        pNext = nil,
        imageMemoryBarrierCount = 1,
        pImageMemoryBarriers = &image_barrier,
    }
    
    vk.CmdPipelineBarrier2(cmd, &dependency_info)
}

init_semaphore_submit_info :: proc(stageMask: vk.PipelineStageFlags2, semaphore: vk.Semaphore) -> vk.SemaphoreSubmitInfo {
	submitInfo := vk.SemaphoreSubmitInfo {
        sType = .SEMAPHORE_SUBMIT_INFO,
        pNext = nil,
        semaphore = semaphore,
        stageMask = stageMask,
        deviceIndex = 0,
        value = 1,
    }
	return submitInfo
}

init_command_buffer_submit_info :: proc(cmd: vk.CommandBuffer) -> vk.CommandBufferSubmitInfo {
	info := vk.CommandBufferSubmitInfo {
        sType = .COMMAND_BUFFER_SUBMIT_INFO,
        pNext = nil,
        commandBuffer = cmd,
        deviceMask = 0,
    }
	return info
}

init_submit_info :: proc(cmd: ^vk.CommandBufferSubmitInfo, signalSemaphoreInfo: ^vk.SemaphoreSubmitInfo, waitSemaphoreInfo: ^vk.SemaphoreSubmitInfo) -> vk.SubmitInfo2 {

    info := vk.SubmitInfo2 {
        sType = .SUBMIT_INFO_2,
        pNext = nil,
        waitSemaphoreInfoCount = waitSemaphoreInfo == nil ? 0 : 1,
        pWaitSemaphoreInfos = waitSemaphoreInfo,
        signalSemaphoreInfoCount = signalSemaphoreInfo == nil ? 0 : 1,
        pSignalSemaphoreInfos = signalSemaphoreInfo,
        commandBufferInfoCount = 1,
        pCommandBufferInfos = cmd,
    }
    return info
}

init_vma :: proc(renderer: ^Vulkan_Renderer) {
    vma_functions := vma.create_vulkan_functions()
    create_info := vma.AllocatorCreateInfo {
        vulkanApiVersion = vk.API_VERSION_1_3,
        physicalDevice = renderer.m_physicalDevice,
        device = renderer.m_device,
        instance = renderer.m_instance,
        pVulkanFunctions = &vma_functions,
    };

    vk_check(vma.CreateAllocator(&create_info, &renderer.m_allocator));
}

init_image_create_info :: proc(format: vk.Format, usage: vk.ImageUsageFlags, extent: vk.Extent3D) -> vk.ImageCreateInfo {
    create_info:= vk.ImageCreateInfo {
        sType = .IMAGE_CREATE_INFO,
        pNext = nil,
        imageType = .D2,
        format = format,
        extent = extent,
        mipLevels = 1,
        arrayLayers = 1,
        samples = {._1},
        tiling = .OPTIMAL,
        usage = usage,
    }
    return create_info
}

init_image_view_create_info :: proc(format: vk.Format, image: vk.Image, aspect_flags: vk.ImageAspectFlags) -> vk.ImageViewCreateInfo {
    create_info:= vk.ImageViewCreateInfo {
        sType = .IMAGE_VIEW_CREATE_INFO,
        pNext = nil,
        format = format,
        image = image,
        viewType = .D2,
        subresourceRange = vk.ImageSubresourceRange {
            baseMipLevel = 0,
            levelCount = 1,
            baseArrayLayer =0,
            layerCount = 1,
            aspectMask = aspect_flags,
        },
    }
    return create_info
}

blit_image :: proc(cmd: vk.CommandBuffer, source, dest: vk.Image, src_size, dest_size: vk.Extent2D) {
    blit_region := vk.ImageBlit2 {
        sType = .IMAGE_BLIT_2,
        pNext = nil,
    }

    blit_region.srcOffsets[1].x = i32(src_size.width)
    blit_region.srcOffsets[1].y = i32(src_size.height)
    blit_region.srcOffsets[1].z = 1

    blit_region.dstOffsets[1].x = i32(dest_size.width)
    blit_region.dstOffsets[1].y = i32(dest_size.height)
    blit_region.dstOffsets[1].z = 1

    blit_region.srcSubresource.aspectMask = {.COLOR}
    blit_region.srcSubresource.baseArrayLayer= 0
    blit_region.srcSubresource.layerCount = 1
    blit_region.srcSubresource.mipLevel = 0

    blit_region.dstSubresource.aspectMask = {.COLOR}
    blit_region.dstSubresource.baseArrayLayer= 0
    blit_region.dstSubresource.layerCount = 1
    blit_region.dstSubresource.mipLevel = 0

    blit_info := vk.BlitImageInfo2 {
        sType = .BLIT_IMAGE_INFO_2,
        pNext = nil, 
        dstImage = dest,
        srcImage = source,
        dstImageLayout = .TRANSFER_DST_OPTIMAL,
        srcImageLayout = .TRANSFER_SRC_OPTIMAL,
        filter = .LINEAR,
        regionCount = 1,
        pRegions = &blit_region,
    }

    vk.CmdBlitImage2(cmd, &blit_info); 
}

init_descriptors :: proc(using renderer: ^Vulkan_Renderer) {
    builder := DescriptorLayoutBuilder{}
    init_descriptorset_builder(&builder)

    poolSizes := []PoolSizeRatio {
        {.STORAGE_IMAGE, 1.0},
    }

    m_globalDescriptorPool = descriptor_pool_init(m_device, 10, poolSizes)
    descriptorset_builder_add_binding(&builder, 0, .STORAGE_IMAGE)
    m_drawImageDescriptorLayout = descriptorset_builder_build(&builder, m_device, {.COMPUTE})

    m_drawImageDescriptors = descriptor_pool_allocate(m_globalDescriptorPool, m_drawImageDescriptorLayout, m_device)

    imgDescriptorInfo := vk.DescriptorImageInfo {
        imageLayout = .GENERAL,
        imageView = m_renderImage.view,
    }

    writeDescriptorSet := vk.WriteDescriptorSet {
        sType = .WRITE_DESCRIPTOR_SET,
        pNext = nil,
        dstBinding = 0,
        dstSet = m_drawImageDescriptors,
        descriptorCount = 1,
        descriptorType = .STORAGE_IMAGE,
        pImageInfo = &imgDescriptorInfo,
    }

    vk.UpdateDescriptorSets(m_device, 1, &writeDescriptorSet, 0, nil)
}

init_pipelines :: proc(using renderer: ^Vulkan_Renderer) {
    init_background_pipelines(renderer)
    log.debug("renderer_init_triangle_pipeline")
    renderer_init_triangle_pipeline(renderer)
} 

init_background_pipelines :: proc(using renderer: ^Vulkan_Renderer) {

    pushConstantRange := vk.PushConstantRange {
        size = size_of(ComputePushConstants),
        offset = 0,
        stageFlags = {.COMPUTE},
    }

    computeLayoutCreateInfo := vk.PipelineLayoutCreateInfo {
        sType = .PIPELINE_LAYOUT_CREATE_INFO,
        pNext = nil,
        pSetLayouts = &m_drawImageDescriptorLayout,
        setLayoutCount = 1,
        pPushConstantRanges = &pushConstantRange,
        pushConstantRangeCount = 1,
    }

    log.debug("creating compute pipeline layout...")
    vk_check(vk.CreatePipelineLayout(m_device, &computeLayoutCreateInfo, nil, &m_gradientPipelineLayout))

    computeDrawShader, ok := shader_load("./bin/shaders/gradient.comp.spv", m_device)
    if !ok do log.panic("failed to load shader")
    defer shader_destroy(m_device, computeDrawShader)

    stageInfo := vk.PipelineShaderStageCreateInfo {
        sType = .PIPELINE_SHADER_STAGE_CREATE_INFO,
        pNext = nil, 
        stage = {.COMPUTE},
        module = computeDrawShader,
        pName = "main",
    }

    computePipelineCreateInfo := vk.ComputePipelineCreateInfo {
        sType = .COMPUTE_PIPELINE_CREATE_INFO,
        pNext = nil,
        layout = m_gradientPipelineLayout,
        stage = stageInfo,
    }

    log.debug("creating compute pipeline...")
    vk_check(vk.CreateComputePipelines(m_device, 0, 1, &computePipelineCreateInfo, nil, &m_gradientPipeline))

    push(&m_deleteQueue, DeleteInfo{
        handle = transmute(vk.Handle)m_gradientPipelineLayout,
        object_name = "m_gradientPipelineLayout",
        delete_fn = proc(self: ^DeleteInfo, device: vk.Device, allocator: vma.Allocator) -> bool {
            vk.DestroyPipelineLayout(device, transmute(vk.PipelineLayout)self.handle.(vk.Handle), nil)
            return true
        },
    })

    push(&m_deleteQueue, DeleteInfo{
        handle = transmute(vk.Handle)m_gradientPipeline,
        object_name = "m_gradientPipeline",
        delete_fn = proc(self: ^DeleteInfo, device: vk.Device, allocator: vma.Allocator) -> bool {
            vk.DestroyPipeline(device, transmute(vk.Pipeline)self.handle.(vk.Handle), nil)
            return true
        },
    })
}

ImmediateCommand :: struct { 
    m_dispatchProc: proc(self: ^ImmediateCommand, cmd: vk.CommandBuffer),
    m_data: rawptr // ptr to extra data 
}

execute_immediately :: proc(using renderer: ^Vulkan_Renderer, execCmd: ImmediateCommand) {
    execCmd := execCmd
    vk_check(vk.ResetFences(m_device, 1, &m_immediateFence))
    vk_check(vk.ResetCommandBuffer(m_immediateCmdBuffer, {}))
    
    cmdBeginInfo := vk.CommandBufferBeginInfo {
        sType = .COMMAND_BUFFER_BEGIN_INFO,
        pNext = nil,
        flags = {.ONE_TIME_SUBMIT},
        pInheritanceInfo = nil,
    }

    vk_check(vk.BeginCommandBuffer(m_immediateCmdBuffer, &cmdBeginInfo))

    execCmd.m_dispatchProc(&execCmd, m_immediateCmdBuffer)
    
    vk_check(vk.EndCommandBuffer(m_immediateCmdBuffer))

    cmdSubmitInfo := init_command_buffer_submit_info(m_immediateCmdBuffer)
    submitInfo := init_submit_info(&cmdSubmitInfo, nil, nil)
    vk_check(vk.QueueSubmit2(m_graphicsQueue, 1, &submitInfo, m_immediateFence))
    vk_check(vk.WaitForFences(m_device, 1, &m_immediateFence, true, ~u64(0)))
}

imgui_init :: proc(using renderer: ^Vulkan_Renderer) {
    poolSizes := []vk.DescriptorPoolSize { 
        { .SAMPLER, 1000 },
		{ .COMBINED_IMAGE_SAMPLER, 1000 },
		{ .SAMPLED_IMAGE, 1000 },
		{ .STORAGE_IMAGE, 1000 },
		{ .UNIFORM_TEXEL_BUFFER, 1000 },
		{ .STORAGE_TEXEL_BUFFER, 1000 },
		{ .UNIFORM_BUFFER, 1000 },
		{ .STORAGE_BUFFER, 1000 },
		{ .UNIFORM_BUFFER_DYNAMIC, 1000 },
		{ .STORAGE_BUFFER_DYNAMIC, 1000 },
		{ .INPUT_ATTACHMENT, 1000 },
    };

    poolInfo := vk.DescriptorPoolCreateInfo {
        sType = .DESCRIPTOR_POOL_CREATE_INFO,
        flags = {.FREE_DESCRIPTOR_SET},
        maxSets = 1000,
        poolSizeCount = u32(len(poolSizes)),
        pPoolSizes = raw_data(poolSizes),
    }

    imguiPool : vk.DescriptorPool
    vk_check(vk.CreateDescriptorPool(m_device, &poolInfo, nil, &imguiPool))

    // Initialize ImGui
    imgui.CHECKVERSION()
    imgui.CreateContext(nil)
    if !imgui_glfw.InitForVulkan(m_window, true) {
        log.panic("failed to init imguiglfw integration")
    }

    imguiVkInitInfo := imgui_vk.InitInfo {
        Instance = m_instance,
        PhysicalDevice = m_physicalDevice,
        Device = m_device,
        Queue = m_graphicsQueue,
        DescriptorPool = imguiPool,
        MinImageCount = 3,
        ImageCount = 3,
        UseDynamicRendering = true,
        ColorAttachmentFormat = m_swapchainImageFormat,
        MSAASamples = {._1},
    }

    gpa :: proc "c" (name: cstring, user_data: rawptr) -> vk.ProcVoidFunction {
        vkInstance := cast(vk.Instance)user_data
        return cast(vk.ProcVoidFunction)glfw.GetInstanceProcAddress(vkInstance, name)
    }

    imgui_vk.LoadFunctions(gpa, cast(rawptr)m_instance)
    imgui_vk.Init(&imguiVkInitInfo, 0)

    execute_immediately(renderer, ImmediateCommand{
        m_dispatchProc = proc(self: ^ImmediateCommand, cmd: vk.CommandBuffer) {
            imgui_vk.CreateFontsTexture(cmd)
        },
    })

    imgui_vk.DestroyFontUploadObjects()
    push(&m_deleteQueue, DeleteInfo {
        handle = transmute(vk.Handle)imguiPool,
        object_name = "imgui descriptor pool",
        delete_fn = proc(self: ^DeleteInfo, device: vk.Device, alloc: vma.Allocator) -> bool {
            vk.DestroyDescriptorPool(device, transmute(vk.DescriptorPool)self.handle.(vk.Handle), nil)
            imgui_vk.Shutdown()
            return true
        },
    })
}

attachment_info_init :: proc(view: vk.ImageView, clearColor: ^vk.ClearValue, layout: vk.ImageLayout) -> vk.RenderingAttachmentInfo {
    colorAttachment := vk.RenderingAttachmentInfo {
        sType = .RENDERING_ATTACHMENT_INFO,
        pNext = nil,
        imageView = view,
        imageLayout = layout,
        loadOp = clearColor != nil ? .CLEAR : .LOAD,
        storeOp = .STORE,
    }

    if clearColor != nil do colorAttachment.clearValue = clearColor^
    return colorAttachment
}

rendering_info_init :: proc(extent: vk.Extent2D, attachment: ^vk.RenderingAttachmentInfo, depthAttachment: ^vk.RenderingAttachmentInfo) -> vk.RenderingInfo {
    renderingInfo := vk.RenderingInfo {
        sType = .RENDERING_INFO,
        pNext = nil,
        renderArea = vk.Rect2D{
            vk.Offset2D{x=0, y=0},
            extent,
        },
        layerCount = 1,
        colorAttachmentCount = 1,
        pColorAttachments = attachment,
        pDepthAttachment = depthAttachment,
        pStencilAttachment = nil,
    }
    return renderingInfo
}

draw_imgui :: proc(using renderer: ^Vulkan_Renderer, cmd: vk.CommandBuffer, targetImageView: vk.ImageView) {
    colorAttachment := attachment_info_init(targetImageView, nil, .GENERAL)
    renderInfo := rendering_info_init(m_swapchainExtent, &colorAttachment, nil)
    
    vk.CmdBeginRendering(cmd, &renderInfo)
    imgui_vk.RenderDrawData(imgui.GetDrawData(), cmd)
    vk.CmdEndRendering(cmd)
}

renderer_init_triangle_pipeline :: proc(using renderer: ^Vulkan_Renderer) {
    triangleFragShader, fragOk := shader_load("./bin/shaders/colored_triangle.frag.spv", m_device)

    if !fragOk do log.panic("failed to load shader")
    defer shader_destroy(m_device, triangleFragShader)

    triangleVertexShader, vertOk := shader_load("./bin/shaders/colored_triangle.vert.spv", m_device)
    if !vertOk do log.panic("failed to load shader")
    defer shader_destroy(m_device, triangleVertexShader)

    pipelineLayoutCreateInfo := vk.PipelineLayoutCreateInfo {
        sType = .PIPELINE_LAYOUT_CREATE_INFO,
        pNext = nil,
    }

    vk_check(vk.CreatePipelineLayout(m_device, &pipelineLayoutCreateInfo, nil, &m_trianglePipelineLayout))

    // build pipeline using our builder 
    pipelineBuilder := pipeline_builder_init()
    pipelineBuilder.m_pipelineLayout = m_trianglePipelineLayout
    pipeline_builder_set_shaders(&pipelineBuilder, triangleVertexShader, triangleFragShader)
    pipeline_builder_set_input_topo(&pipelineBuilder, .TRIANGLE_LIST)
    pipeline_builder_set_polygon_mode(&pipelineBuilder, .FILL)
    pipeline_builder_set_cull_mode(&pipelineBuilder, {}, .CLOCKWISE)
    pipeline_builder_set_multisampling_none(&pipelineBuilder)
    pipeline_builder_disable_blending(&pipelineBuilder)
    pipeline_builder_disable_depthtest(&pipelineBuilder)
    pipeline_builder_set_color_attachment_format(&pipelineBuilder, m_renderImage.format)
    pipeline_builder_set_depth_format(&pipelineBuilder, .UNDEFINED)
    m_trianglePipeline = pipeline_builder_build(&pipelineBuilder, m_device)
    
    push(&m_deleteQueue, DeleteInfo {
        object_name = "m_trianglePipelineLayout",
        handle = transmute(vk.Handle)m_trianglePipelineLayout,
        delete_fn = proc(self: ^DeleteInfo, device: vk.Device, alloc: vma.Allocator) -> bool {
            vk.DestroyPipelineLayout(device, transmute(vk.PipelineLayout)self.handle.(vk.Handle), nil)
            return true
        },
    })

}

renderer_draw_geometry :: proc(using renderer: ^Vulkan_Renderer, cmd: vk.CommandBuffer) {
    colorAttachment := attachment_info_init(m_renderImage.view, nil, .GENERAL) // NOTE: I think this should be color attachment optimal, not general?
    renderingInfo := rendering_info_init(m_swapchainExtent, &colorAttachment, nil)

    vk.CmdBeginRendering(cmd, &renderingInfo)
    vk.CmdBindPipeline(cmd, .GRAPHICS, m_trianglePipeline)

    viewport := vk.Viewport {
        x = 0, 
        y = 0,
        width = f32(m_swapchainExtent.width),
        height = f32(m_swapchainExtent.height),
        minDepth = 0.0,
        maxDepth = 1.0,
    }

    vk.CmdSetViewport(cmd, 0, 1, &viewport)

    scissor := vk.Rect2D {
        offset = {0, 0},
        extent = {
            width = m_swapchainExtent.width,
            height = m_swapchainExtent.height,
        },
    }

    vk.CmdSetScissor(cmd, 0, 1, &scissor)
    vk.CmdDraw(cmd, 3, 1, 0, 0)
    vk.CmdEndRendering(cmd)
}








