package engine;
import vk "vendor:vulkan"

// TODO(nice_sprite): does this really need to be a struct? 
// there is literally no benefit from making this a struct lol 
DescriptorLayoutBuilder :: struct {
    bindings: [dynamic]vk.DescriptorSetLayoutBinding,
}

init_descriptorset_builder :: proc(builder: ^DescriptorLayoutBuilder) {
    builder.bindings = make([dynamic]vk.DescriptorSetLayoutBinding)
}

deinit_descriptorset_builder :: proc(using builder: ^DescriptorLayoutBuilder) {
    free(&bindings)
}

descriptorset_builder_add_binding :: proc(using builder: ^DescriptorLayoutBuilder, binding: u32, type: vk.DescriptorType) {
    newbind := vk.DescriptorSetLayoutBinding {
        binding = binding,
        descriptorType = type,
        descriptorCount = 1,
    }
    append(&bindings, newbind);
}

descriptorset_builder_wipe :: proc(using builder: ^DescriptorLayoutBuilder) {
    clear(&bindings)
}

descriptorset_builder_build :: proc(using builder: ^DescriptorLayoutBuilder, device: vk.Device, stage_flags: vk.ShaderStageFlags) -> vk.DescriptorSetLayout {
    for &b in bindings do b.stageFlags |= stage_flags

    create_info := vk.DescriptorSetLayoutCreateInfo {
        sType = .DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        pNext = nil,
        pBindings = raw_data(bindings),
        bindingCount = cast(u32)len(bindings),
        flags = {},
    }

    set : vk.DescriptorSetLayout
    vk_check(vk.CreateDescriptorSetLayout(device, &create_info, nil, &set))
    return set
}


PoolSizeRatio :: struct {
    type: vk.DescriptorType,
    ratio: f32,
}


descriptor_pool_init :: proc(device: vk.Device, maxSets: u32, poolRatios: []PoolSizeRatio) -> vk.DescriptorPool {
    poolSizes := make([dynamic]vk.DescriptorPoolSize)
    defer delete(poolSizes)
    for ratio in poolRatios {
        append(&poolSizes, vk.DescriptorPoolSize{ type = ratio.type, descriptorCount = cast(u32)(ratio.ratio * cast(f32)maxSets)})
    }

    poolInfo := vk.DescriptorPoolCreateInfo {
        sType = .DESCRIPTOR_POOL_CREATE_INFO,
        maxSets = maxSets,
        poolSizeCount = u32(len(poolSizes)),
        pPoolSizes = raw_data(poolSizes),
    }
    pool : vk.DescriptorPool
    vk_check(vk.CreateDescriptorPool(device, &poolInfo, nil, &pool))
    return pool
}

descriptor_pool_reset :: proc(pool: vk.DescriptorPool, device: vk.Device) {
    vk_check(vk.ResetDescriptorPool(device, pool, {}))
}

descriptor_pool_destroy :: proc(pool: vk.DescriptorPool, device: vk.Device) {
    vk.DestroyDescriptorPool(device, pool, nil)
}

descriptor_pool_allocate :: proc(pool: vk.DescriptorPool, layout: vk.DescriptorSetLayout, device: vk.Device) -> vk.DescriptorSet {
    layout := layout
    allocInfo := vk.DescriptorSetAllocateInfo {
        sType = .DESCRIPTOR_SET_ALLOCATE_INFO,
        pNext = nil,
        descriptorPool = pool,
        descriptorSetCount = 1,
        pSetLayouts = &layout,
    }
    descriptor_set : vk.DescriptorSet
    vk_check(vk.AllocateDescriptorSets(device, &allocInfo, &descriptor_set))
    return descriptor_set
}
