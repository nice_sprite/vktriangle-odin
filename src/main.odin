package main
import "core:fmt"
import "engine"
import "vendor:glfw"
import "core:log"
import "core:os"
import imgui "external:odin-imgui"
import imgui_glfw "external:odin-imgui/imgui_impl_glfw"
import imgui_vk "external:odin-imgui/imgui_impl_vulkan"

WAYLAND :: #config(WAYLAND, false)
X11 :: #config(X11, false)
RENDERDOC :: #config(RENDERDOC, false)

main :: proc() {

    handle, ok := os.open("log", os.O_CREATE | os.O_RDWR | os.O_TRUNC)
    defer os.close(handle)
    fmt.print("created logger", handle, '\n');
    //context.logger = log.create_file_logger(handle, log.Level.Debug, log.Options{ .Level, .Line, .Procedure, .Short_File_Path, .Time })
    context.logger = log.create_console_logger(log.Level.Debug, log.Options{ .Terminal_Color, .Level, .Line, .Procedure, .Short_File_Path, .Time })

    glfw.InitHint(glfw.CLIENT_API, glfw.NO_API)
    if !glfw.Init() || !glfw.VulkanSupported() {
        log.error("failed to init glfw or vulkan is not supported", glfw.GetError())
        return
    }
    defer glfw.Terminate()

    glfw.WindowHint(glfw.CLIENT_API, glfw.NO_API)
    window := glfw.CreateWindow(800, 600, "Gooba jooba", nil, nil)
    defer glfw.DestroyWindow(window)
    

    required : [dynamic]cstring
    append(&required, "VK_KHR_surface")
    when ODIN_OS == .Windows do append(&required, "")
    when ODIN_OS == .Linux && WAYLAND {
        append(&required, "VK_KHR_wayland_surface")
    }
    when ODIN_OS == .Linux && X11 {
        append(&required, "VK_KHR_xcb_surface")
    }

    when ODIN_DEBUG {
        append(&required, "VK_EXT_debug_utils")
    }
    
    renderer : engine.Vulkan_Renderer;
    engine.init_renderer(&renderer, window, required[:])
    for !glfw.WindowShouldClose(window) {
        glfw.PollEvents()
        if glfw.GetKey(window, glfw.KEY_ESCAPE) > 0 do glfw.SetWindowShouldClose(window, true)

        imgui_glfw.NewFrame()
        imgui_vk.NewFrame()
        imgui.NewFrame()
        
        imgui.ShowDemoWindow(nil)

        imgui.Render()
        engine.draw(&renderer)
    }
    defer engine.deinit_renderer(&renderer)
}
