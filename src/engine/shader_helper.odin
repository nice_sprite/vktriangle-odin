package engine;
import vk "vendor:vulkan"
import "core:os"
import "core:path/filepath"
import "core:fmt"


shader_load :: proc(name: string, device: vk.Device) -> (vk.ShaderModule, bool) {
    shader_bytes, ok := os.read_entire_file_from_filename(name)
    if ok {
        createInfo := vk.ShaderModuleCreateInfo {
            sType = .SHADER_MODULE_CREATE_INFO,
            pNext = nil,
            codeSize = len(shader_bytes),
            pCode = transmute([^]u32)raw_data(shader_bytes),
        }

        module : vk.ShaderModule
        vk_check(vk.CreateShaderModule(device, &createInfo, nil, &module))
        return module, true
    }

    return {}, false
}


shader_destroy :: proc(device: vk.Device, shader: vk.ShaderModule) {
    vk.DestroyShaderModule(device, shader, nil)
}


